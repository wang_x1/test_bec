import time
import threading

from ophyd import (
    Device,
    DeviceStatus,
)

from ophyd_devices.interfaces.base_classes.psi_detector_base import CustomDetectorMixin, PSIDetectorBase

def delayed_complete(task, delay, status):
    time.sleep(delay)
    timestamp = time.ctime()
    print(f'{timestamp} {task} completed')
    status.set_finished()

class TestProfileMove(Device):
    USER_ACCESS = ['build_profile', 'execute_profile', 'readback_profile']

    def build_profile(self):
        status = DeviceStatus(self)
        #thread = threading.Thread(target=delayed_complete, args=('build', 0.01, status), daemon=True)
        #thread.start()
        status.set_exception(Exception('Failed to build profile'))
        return status

    def execute_profile(self):
        status = DeviceStatus(self)
        thread = threading.Thread(target=delayed_complete, args=('execute', 3, status), daemon=True)
        thread.start()

        return status

    def readback_profile(self):
        status = DeviceStatus(self)
        thread = threading.Thread(target=delayed_complete, args=('readback', 1, status), daemon=True)
        thread.start()

        return status

class TestDetectorMixin(CustomDetectorMixin):
    def __init__(self, *_args, parent: Device = None, **_kwargs):
        super().__init__(*_args, parent=parent, **_kwargs)

    def on_trigger(self):
        status = DeviceStatus(self.parent)
        thread = threading.Thread(target=delayed_complete, args=('on_trigger', 1, status), daemon=True)
        thread.start()

        return status

    def on_complete(self):
        raise Exception('Failed to complete')

        if self.parent.scaninfo.scan_type == 'step':
            delay = 1
        else:
            delay = 10
        status = DeviceStatus(self.parent)
        thread = threading.Thread(target=delayed_complete, args=('on_complete', delay, status), daemon=True)
        thread.start()
        #status.set_exception(TimeoutError())
        return status

class TestDetector(PSIDetectorBase):
    custom_prepare_cls = TestDetectorMixin
    