from bec_lib import messages
from bec_lib.endpoints import MessageEndpoints
from bec_lib.logger import bec_logger
from bec_server.scan_server.scans import ScanArgType, AsyncFlyScanBase

logger = bec_logger.logger

class TestFlyScan(AsyncFlyScanBase):
    scan_name = 'test_flyscan'
    scan_type = 'fly'
    arg_input = {
        'controller': ScanArgType.DEVICE,
    }
    arg_bundle_size = {"bundle": len(arg_input), "min": 1, "max": 1}
    required_kwargs = []
    use_scan_progress_report = False

    def __init__(self, controller, *args, **kwargs):
        self.controller = controller
        super().__init__(**kwargs)

    @property
    def monitor_sync(self):
        return self.controller

    def scan_core(self):
        status = yield from self.stubs.send_rpc_and_wait(self.controller, 'build_profile')
        logger.info('wait profile build')
        status.wait()

        status = yield from self.stubs.send_rpc_and_wait(self.controller, 'execute_profile')
        logger.info('wait profile execute')
        status.wait()

        status = yield from self.stubs.send_rpc_and_wait(self.controller, 'readback_profile')
        logger.info('wait profile readback')
        status.wait()

        self.num_pos = 102

        #self._send_data_to_bec()

    def _send_data_to_bec(self) -> None:
        """Sends bundled data to BEC"""
        #metadata = self.parent.scaninfo.scan_msg.metadata
        #metadata.update({"async_update": "append", "num_lines": self.parent.num_lines.get()})
        metadata = {"async_update": "append", "num_lines": self.num_pos}
        msg = messages.DeviceMessage(
            signals={'motor1': {'value': list(range(self.num_pos)), 'timestamp': '0'} }, metadata=metadata
        )

        self.stubs.connector.xadd(
            topic=MessageEndpoints.device_async_readback(
                scan_id=self.scan_id, device='motor1'
            ),
            msg_dict={"data": msg},
            expire=1800,
        )

